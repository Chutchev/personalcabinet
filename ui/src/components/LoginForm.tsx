import React from "react";
import {
  Button,
  Card,
  CardContent,
  Paper,
  Grid,
  TextField,
  Toolbar,
} from "@mui/material";
import { Box } from "@mui/system";

interface LoginFormState {
  email: string;
  password: string;
}

const LoginForm: React.FC<{}> = () => {
  const [state, setState] = React.useState<LoginFormState>({
    email: "",
    password: "",
  });
  const handelForm = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({
      ...state,
      [event.currentTarget.name]: event.currentTarget.value,
    });
  };
  const handelFormSubmit = () => {
    console.log(state);
  };
  return (
    <Paper sx={{ width: "300px" }}>
      <Toolbar
        sx={{
          background: "#363C49",
          color: "#F0F2F5",
          font: "Open Sans",
          fontSize: "36",
          fontWeight: "600",
        }}
      >
        РЦР
      </Toolbar>
      <Box sx={{ display: "flex", flexDirection: "column" }}>
        <TextField fullWidth label="fullWidth" id="fullWidth" />
        <TextField fullWidth label="fullWidth" id="fullWidth" />
      </Box>
    </Paper>
  );
};

export default LoginForm;
