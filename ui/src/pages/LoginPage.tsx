import { Grid } from "@mui/material";
import React from "react";
import { LoginForm } from "../components";

const LoginPage: React.FC<{}> = () => {
  return (
    <Grid container>
      <Grid item>
        <LoginForm />
      </Grid>
      <Grid item>
        <LoginForm />
      </Grid>
      <Grid item>
        <LoginForm />
      </Grid>
      <Grid item>
        <LoginForm />
      </Grid>
    </Grid>
  );
};

export default LoginPage;
