import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Layout from "./pages/Layout";
import { Container, Grid } from "@mui/material";
import LoginPage from "./pages/LoginPage";
import NewsList from "./components/News/NewsList";
import { INews } from "./components/News/types";
import { LoginForm } from "./components";

const news: INews[] = [
  {
    id: 1,
    title: "Новость 1",
    author: "Автор 1",
    date: new Date("2021-10-23"),
    photo: "/img/unknown.png",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor " +
          "incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud " +
          "exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure " +
          "dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. " +
          "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim " +
          "id est laborum.",
    attachments: ["1", "2"],
  },
  {
    id: 2,
    title: "Новость 2",
    author: "Автор 1",
    date: new Date("2021-09-23"),
    photo: "/img/unknown.png",
    text: "текст новости2",
    attachments: ["5", "3"],
  },
  {
    id: 3,
    title: "Новость 3",
    author: "Автор 2",
    date: new Date("2021-10-23"),
    photo: "/img/unknown.png",
    text: "текст новости3",
    attachments: ["6"],
  },
];

function App() {
  const isAuth = 1;
  return (
    <div className="App">
      {isAuth ? (
        <div className="App">
          <Layout>
            <NewsList news={news} />
          </Layout>
        </div>
      ) : (
        <Grid
          container
          sx={{
            minHeight: "100vh",
            justifyContent: "center",
            alignContent: "center",
          }}
          direction="column"
        >
          <Grid item>
            <LoginForm />
          </Grid>
        </Grid>
      )}
    </div>
  );
}

export default App;
