import React from "react";
import {
  AppBar,
  Button,
  Container,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";

const Layout: React.FC<{}> = ({ children }) => {
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              flex: 1,
            }}
          >
            <Typography variant="h6" component="div">
              РЦР
            </Typography>
            <Button color="inherit">Сидоров Иван</Button>
          </div>
        </Toolbar>
      </AppBar>
      <main>
        <Container maxWidth="xl">{children}</Container>
      </main>
    </div>
  );
};

export default Layout;
