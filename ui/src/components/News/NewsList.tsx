import React from 'react';
import {INewsList} from "./types";
import NewsItem from "./NewsItem";
import {Box, Divider, Grid, IconButton, Paper, Typography} from "@mui/material";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

const NewsList: React.FC<INewsList> = (props) => {
    return (
        <Paper>
            <Typography variant="h5" component="div" align={'left'} color={"#354052"} sx={
                {padding: "15px 24px 15px 24px",
                fontSize: 22,
                fontFamily: "openSans"}
            }>
                Новости <IconButton><AddCircleOutlineIcon sx={{color: "#1890FF"}}/></IconButton>
            </Typography>
            <Divider />
            <Grid container spacing={"26px"}>
            {props.news.map((item)=>{
                return <Grid item md={12}>
                    <NewsItem
                        key = {`news-item-${item.id}`}
                        id={item.id}
                        title={item.title}
                        author={item.author}
                        date={item.date}
                        photo={item.photo}
                        text={item.text}
                        attachments={item.attachments}/>
                </Grid>
            })}
            </Grid>
        </Paper>
    );
}

export default NewsList;