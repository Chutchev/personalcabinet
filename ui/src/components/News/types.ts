export interface INewsList {
    news: INews[];
}

export interface INews {
    id: number;
    title: string;
    author: string;
    date: Date;
    photo: string;
    text: string;
    attachments: string[];
}