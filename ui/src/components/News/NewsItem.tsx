import React from "react";
import {INews} from "./types";
import {
    Box,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia, Grid,
    Typography,
} from "@mui/material";

const NewsList: React.FC<INews> = (props) => {
    return (
        <Card sx={{maxWidth: "100%", display: "flex"}}>
            <CardMedia
                component="img"
                height="140"
                width="140"
                image={process.env.PUBLIC_URL + props.photo}
                alt="green iguana"
                sx={{display: "flex", height: "140px", width: "140px", margin: "30px", color: "black"}}
            />
            <Grid container direction={"column"} sx={{textAlign: "start", fontFamily: "Roboto", color: "black"}}>
                <Grid item><CardHeader title={props.title}/></Grid>
                <Grid item> <CardContent sx={{display: "flex", flex: 1}}>
                    <Typography variant="body2" color="text.secondary" sx={{fontFamily: "Roboto", color: "black", fontSize: "20px"}}>
                        {props.text}
                    </Typography>
                </CardContent></Grid>
                <Grid item>
                    <Typography variant="body2" color="text.secondary" sx={{fontFamily: "Roboto", color: "black"}}>
                        Дата: {props.date.toDateString()}
                    </Typography>
                </Grid>
            </Grid>

        </Card>
    );
};

export default NewsList;
